package com.example.toik_sudoku.repository;

import com.opencsv.CSVReader;
import com.example.toik_sudoku.dto.SudokuDto;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

@Repository
public class SudokuRepositoryImpl implements SudokuRepository {
    private SudokuDto table;
    private static final String CSV = "sudoku.csv";

    public SudokuRepositoryImpl() {
        ReadCsv();
    }

    public void ReadCsv(){

        List<List<String>> recordsFromCsv = new ArrayList<>();
        try(CSVReader csvReader = new CSVReader(new FileReader(CSV))){
            String[] values;
            while((values = csvReader.readNext()) != null){
                recordsFromCsv.add(Arrays.asList(values));
            }
        } catch( FileNotFoundException e){} catch (IOException e) {
            e.printStackTrace();
        }
        intoNumbers(recordsFromCsv);

    }

    public void intoNumbers(List<List<String>> recordsFromCsv){
        List<List<Integer>> tableValues = new ArrayList<>();
        for(List<String> list:recordsFromCsv){
            List<Integer> holder = new ArrayList<>();
            for(String item:list){
                holder.add(Integer.parseInt(item));
            }
            tableValues.add(holder);
        }
        table = new SudokuDto(tableValues);
    }

    @Override
    public SudokuDto getTable() {
        return table;
    }

}
