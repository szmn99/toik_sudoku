package com.example.toik_sudoku.repository;

import com.example.toik_sudoku.dto.SudokuDto;
import org.springframework.stereotype.Service;

@Service
public interface SudokuRepository {
    void ReadCsv();
    SudokuDto getTable();

}
