package com.example.toik_sudoku.rest;


import com.example.toik_sudoku.service.SudokuService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequestMapping(value = "/api/sudoku")
public class SudokuApiController {

    @Autowired
    SudokuService sudokuService;

    @PostMapping(value = "verify")
    public ResponseEntity verify(){
        if(sudokuService.verifySudoku()){
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(sudokuService.getWrongSudokuDto(), HttpStatus.BAD_REQUEST);
    }
}
