package com.example.toik_sudoku.dto;

import java.util.List;

public class SudokuDto {
    public SudokuDto(){ }

    public SudokuDto(List<List<Integer>> table){
        this.table = table;
    }

    private List<List<Integer>> table;

    public List<List<Integer>> getTable() {
        return table;
    }

    public void setTable(List<List<Integer>> table) {
        this.table = table;
    }

}
