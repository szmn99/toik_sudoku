package com.example.toik_sudoku.dto;

import java.util.List;

public class WrongSudokuDto {

    private List<Integer> lineIds;
    private List<Integer> columnIds;
    private List<Integer> squareIds;

    public WrongSudokuDto() {
    }

    public WrongSudokuDto(List<Integer> line, List<Integer> column, List<Integer> square) {
        this.lineIds = line;
        this.columnIds = column;
        this.squareIds = square;
    }

    public List<Integer> getLine() {
        return lineIds;
    }

    public List<Integer> getColumn() {
        return columnIds;
    }

    public void setLine(List<Integer> line) {
    }

    public void setColumn(List<Integer> column) {
    }

    public void setSquare(List<Integer> square) {
    }

    public List<Integer> getSquare() {
        return squareIds;
    }
}
