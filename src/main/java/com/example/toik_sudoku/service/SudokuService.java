package com.example.toik_sudoku.service;

import com.example.toik_sudoku.dto.SudokuDto;
import com.example.toik_sudoku.dto.WrongSudokuDto;


public interface SudokuService {
    SudokuDto getTable();
    boolean verifySudoku();
    WrongSudokuDto getWrongSudokuDto();

}