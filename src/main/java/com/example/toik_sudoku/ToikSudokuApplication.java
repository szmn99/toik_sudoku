package com.example.toik_sudoku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToikSudokuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToikSudokuApplication.class, args);
	}

}
